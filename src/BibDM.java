import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.SortedSet;

public class BibDM{

  /**
   * Ajoute deux entiers
   * @param a le premier entier à ajouter
   * @param b le deuxieme entier à ajouter
   * @return la somme des deux entiers
   */
  public static Integer plus(Integer a, Integer b){
    return a+b;
  }


  /**
   * Renvoie la valeur du plus petit élément d'une liste d'entiers
   * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
   * @param liste
   * @return le plus petit élément de liste
   */
  public static Integer min(List<Integer> liste){
    Integer res = null;
    for (Integer i : liste) {
      if (res == null || i < res) {
        res = i;
      }
    }
    return res;
  }


  /**
   * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
   * @param valeur
   * @param liste
   * @return true si tous les elements de liste sont plus grands que valeur.
   */
  public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
    boolean plusPetit = true;
    for (int i = 0; i < liste.size() && plusPetit; ++i) {
      if (liste.get(i).compareTo(valeur) <= 0)
        plusPetit = false;
    }
    return plusPetit;
  }


  /**
   * Intersection de deux listes données par ordre croissant.
   * @param liste1 une liste triée
   * @param liste2 une liste triée
   * @return une liste triée avec les éléments communs à liste1 et liste2
   */
  public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
    List<T> liste = new ArrayList<>();
    int i1 = 0, i2 = 0;
    T prec = null;
    while (i1 < liste1.size() && i2 < liste2.size()) {
      T val1 = liste1.get(i1), val2 = liste2.get(i2);
      if (prec == null || val1 != prec) {
        if (val1.compareTo(val2) == 0) {
          liste.add(val1);
          prec = val1;
          ++i1; ++i2;
        } else if (val1.compareTo(val2) < 0) {
          ++i1;
        } else {
          ++i2;
        }
      } else { ++i1;}
    }
    return liste;
  }


  /**
   * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
   * @param texte une chaine de caractères
   * @return une liste de mots, correspondant aux mots de texte.
   */
  public static List<String> decoupe(String texte){
    List<String> res = new ArrayList<>();
    String mot = "";
    for (int i = 0; i < texte.length(); ++i) {
      if (texte.charAt(i) != ' ') {
        mot += texte.charAt(i);
      } else {
        if (!mot.equals("")) {
          res.add(mot);
          mot = "";
        }
      }
    }
    if (!mot.equals(""))
      res.add(mot);
    return res;
  }


  /**
   * Renvoie le mot le plus présent dans un texte.
   * @param texte une chaine de caractères
   * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
   */

  public static String motMajoritaire(String texte){
    List<String> listeMots = decoupe(texte);
    Collections.sort(listeMots);
    Map<String, Integer> occur = new HashMap<>();
    for (String mot : listeMots) {
      if (occur.containsKey(mot)) {
        occur.put(mot, occur.get(mot) + 1);
      } else {
        occur.put(mot, 0);
      }
    }
    String motMax = null;
    Integer occMax = 0;
    Set<String> ensMots = occur.keySet();
    List<String> motsTries = new ArrayList<>();
    for (String mot : ensMots) {
      motsTries.add(mot);
    }
    Collections.sort(motsTries);
    for (String mot : motsTries) {
      if (occur.get(mot) > occMax) {
        motMax = mot;
        occMax = occur.get(mot);
      }
    }
    return motMax;
  }


  /**
   * Permet de tester si une chaine est bien parenthesée
   * @param chaine une chaine de caractères composée de ( et de )
   * @return true si la chaine est bien parenthèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
   */
  public static boolean bienParenthesee(String chaine){
    int cptParentheses = 0;
    for (int i = 0; i < chaine.length(); ++i) {
      if (chaine.charAt(i) == '(')
        cptParentheses++;

      else if (chaine.charAt(i) == ')') {
        if (cptParentheses == 0)
          return false;
        else
          cptParentheses--;
      }
    }
    return cptParentheses == 0;
  }


  /**
   * Permet de tester si une chaine est bien parenthesée
   * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
   * @return true si la chaine est bien parenthèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors que ([]) est bien parenthèsée.
   */
  public static boolean bienParentheseeCrochets(String chaine){
    List<String> listePC = new ArrayList<>();
    for (int i = 0; i < chaine.length(); ++i) {
      String elem = chaine.substring(i, i+1);
      System.out.println(elem);
      if (elem.equals("(") || elem.equals("["))
        listePC.add(elem);
      else {
        if (listePC.size() == 0)
          return false;

        int last = listePC.size() - 1;
        if (elem.equals(")")) {
          if (listePC.get(last).equals("("))
            listePC.remove(last);
          else
            return false;
        }
        else if (elem.equals("]")) {
          if (listePC.get(last).equals("["))
            listePC.remove(last);
          else
            return false;
        }
      }
    }
    return (listePC.size() == 0);
  }


  /**
   * Recherche par dichotomie d'un élément dans une liste triée
   * @param liste, une liste triée d'entiers
   * @param valeur un entier
   * @return true si l'entier appartient à la liste.
   */
  public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
    int a = 0, b = liste.size() - 1;
    while (a < b) {
      int c = (a + b) / 2;
      if (liste.get(c).compareTo(valeur) < 0)
        a = c + 1;
      else
        b = c;
    }
    return (a < liste.size() && valeur.equals(liste.get(a)));
  }
}
